#include<stdio.h>

struct student{

  char name[50];
  char subject[20];
  float marks;
};

int main()
{
    int p, q;
    struct student std[10];

    printf("Number of students whose details you want to record: ");
    scanf("%d", &q);


    for (p=0;p<q; p++)
    {
        printf("\nPlease enter the name of the student %d\n", p+1);
        scanf("%s", &std[p].name);
        printf("\nPlease enter the subject of the student %d\n", p+1);
        scanf("%s", &std[p].subject);
        printf("\nPlease enter the marks obtained by the student %d\n", p+1);
        scanf("%f", &std[p].marks);

    }
   printf("\nList of student details\n\n");

   for(p=0; p<q; p++)
   {
       printf("Name: %s\n", std[p].name);
       printf("Subject: %s\n", std[p].subject);
       printf("Marks: %.1f\n", std[p].marks);
       printf("\n");
   }

   return 0;
}
